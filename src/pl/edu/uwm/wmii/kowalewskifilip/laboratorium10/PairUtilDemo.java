package pl.edu.uwm.wmii.kowalewskifilip.laboratorium10;

public class PairUtilDemo {
    public static void main(String[] args){
        Pair<Integer> para = new Pair<Integer>(11, 2);
        System.out.println("first = " + para.getFirst());
        System.out.println("second = " + para.getSecond());
        para = PairUtil.swap(para);
        System.out.println("first = " + para.getFirst());
        System.out.println("second = " + para.getSecond());
    }

}
