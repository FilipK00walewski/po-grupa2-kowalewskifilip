package pl.edu.uwm.wmii.kowalewskifilip.laboratorium10;

import java.time.LocalDate;

class ArrayUtilDemo{
    public static void main(String[] args){
        int n = 6;
        ArrayUtil szukana = new ArrayUtil(3);
        ArrayUtil szukana2 = new ArrayUtil(LocalDate.of(2000, 06, 30));
        ArrayUtil tab[] = new ArrayUtil[n];
        ArrayUtil tab2[] = new ArrayUtil[n];

        for (int i = 0; i < n; i++){
            tab[i] = new ArrayUtil(i);
            tab2[i] = new ArrayUtil(LocalDate.of(2019, 01, i + 1));
        }

        boolean odp, odp2;
        odp = ArrayUtil.isSorted(tab);
        odp2 = ArrayUtil.isSorted(tab2);

        for (int i = 0; i < tab.length; i++) {
            System.out.print(tab[i].getElement() + " ");
        }System.out.println(odp);

        for (int i = 0; i < tab.length; i++) {
            System.out.print(tab2[i].getElement() + " ");
        }System.out.println(odp2);

        tab[0].setElement(400);
        tab[5].setElement(2);
        tab[4].setElement(-2);
        tab2[4].setElement(LocalDate.of(2000, 06, 30));

        odp = ArrayUtil.isSorted(tab);
        odp2 = ArrayUtil.isSorted(tab2);

        for (int i = 0; i < tab.length; i++) {
            System.out.print(tab[i].getElement() + " ");
        }System.out.println(odp);

        for (int i = 0; i < tab.length; i++) {
            System.out.print(tab2[i].getElement() + " ");
        }System.out.println(odp2);

        //zadanie 4
        System.out.println("zadanie 4: ");
        System.out.println(ArrayUtil.binSearch(tab, szukana));
        System.out.println(ArrayUtil.binSearch(tab2, szukana2));

        ArrayUtil.selectionSort(tab);
        ArrayUtil.selectionSort(tab2);
        System.out.println("Sortowanie przez wybieranie:");

        for (int i = 0; i < tab.length; i++) {
            System.out.print(tab[i].getElement() + " ");
        }System.out.println("");

        tab[0].setElement(400);
        tab[5].setElement(2);
        tab[4].setElement(-2);
        System.out.println("Sortowanie przez wybieranie LocalDate:");
        for (int i = 0; i < tab.length; i++) {
            System.out.print(tab2[i].getElement() + " ");
        }System.out.println("");

        tab[0].setElement(400);
        tab[5].setElement(2);
        tab[4].setElement(-2);

        for (int i = 0; i < tab.length; i++) {
            System.out.print(tab[i].getElement() + " ");
        }System.out.println("");
        System.out.println("Sortowanie przez scalanie:");
        ArrayUtil.sort(tab);
        for (int i = 0; i < tab.length; i++) {
            System.out.print(tab[i].getElement() + " ");
        }
    }


}




