package pl.edu.uwm.wmii.kowalewskifilip.laboratorium10;

import java.util.*;

public class ArrayUtil<T> implements Comparable<ArrayUtil<T>>{
    private T element;
    ArrayUtil<T> e;

    public ArrayUtil(T element){
        this.element = element;
    }

    public void setElement(T element){
        this.element = element;
    }

    public String toString(){
        return element.toString();
    }

    public T getElement(){
        return element;
    }


    public static <T extends Comparable<T>> boolean isSorted(T[] tab){
        T M;
        M = tab[0];
        for(int i = 1; i < tab.length; i ++){
            if(tab[i] != null) {

                if (M.toString().compareTo(tab[i].toString()) > 0){
                    return false;
                }
                M = tab[i];
            }
        }
        return true;
    }

    public int compareTo(ArrayUtil<T> o){
        return e.compareTo(o.e);
    }


    public static  <T extends Comparable<T>> int binSearch(T tab[], T szukana){

        for(int i = 0; i < tab.length; i++){
            if(szukana.toString().compareTo(tab[i].toString()) == 0) return i;
        }
        return -1;
    }

    public static <T extends Comparable<T>> void selectionSort(T tab[]){
        int n = tab.length;
        int minIndex;

        for(int i = 0; i < n - 1; i++){
            minIndex = i;
            for(int j = i + 1; j < n; j++){
                if(tab[minIndex].toString().compareTo(tab[j].toString()) > -1){
                    minIndex = j;
                }
            }
            if(minIndex != i){
                T temp = tab[minIndex];
                tab[minIndex] = tab[i];
                tab[i] = temp;
            }
        }
    }

//*****************************************************

    public static <T extends Comparable<T>> void sort(T[] a) {
        mergesort(a, 0, a.length-1);
    }


    private static <T extends Comparable<T>> void mergesort (T[] a, int i, int j) {
        if (j-i < 1) return;
        int mid = (i+j)/2;
        mergesort(a, i, mid);
        mergesort(a, mid+1, j);
        merge(a, i, mid, j);
    }


    private static <T extends Comparable<T>> void  merge(T[] a, int p, int mid, int q) {

        Object[] tmp = new Object[q-p+1];
        int i = p;
        int j = mid+1;
        int k = 0;
        while (i <= mid && j <= q) {
            if (a[i].toString().compareTo(a[j].toString())<=0)
                tmp[k] = a[i++];
            else
                tmp[k] = a[j++];
            k++;
        }
        if (i <= mid && j > q) {
            while (i <= mid)
                tmp[k++] = a[i++];
        } else {
            while (j <= q)
                tmp[k++] = a[j++];
        }
        for (k = 0; k < tmp.length; k++) {
            a[k+p] = (T)(tmp[k]);
        }
    }

}

interface Comparable<T>{

}



