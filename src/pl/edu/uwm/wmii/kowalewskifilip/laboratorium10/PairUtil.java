package pl.edu.uwm.wmii.kowalewskifilip.laboratorium10;

public class PairUtil<T> extends Pair{

    public static <T> Pair swap(Pair para){
        para = new Pair(para.getSecond(), para.getFirst());
        return para;
    }

}

