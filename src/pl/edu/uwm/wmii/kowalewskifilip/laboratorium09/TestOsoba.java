package pl.edu.uwm.wmii.kowalewskifilip.laboratorium09;

import java.util.*;
import java.time.LocalDate;
import pl.edu.uwm.wmii.kowalewskifilip.laboratorium09.pl.imiajd.kowalewski.Osoba;


public class TestOsoba {
    public static void main(String[] args) {

        ArrayList<Osoba> grupa = new ArrayList<Osoba>();

        grupa.add(new Osoba("Nowak", LocalDate.of(3000, 11, 14)));
        grupa.add(new Osoba("Kowalski", LocalDate.of(1999, 10, 4)));
        grupa.add(new Osoba("Wojtyla", LocalDate.of(1930, 2, 4)));
        grupa.add(new Osoba("Nowak", LocalDate.of(1999, 10, 4)));
        grupa.add(new Osoba("Tusk", LocalDate.of(1990, 11, 15)));
        grupa.add(new Osoba("Kowalewski", LocalDate.of(1997, 6, 30)));

        System.out.println("Przed sortowaniu: " + grupa);

        Collections.sort(grupa, new Comparator<Osoba>() {
            @Override
            public int compare(Osoba o1, Osoba o2) {
                return o1.compareTo(o2);
            }
        });

        System.out.println("Po sortowaniu: " + grupa);
    }

}






