package pl.edu.uwm.wmii.kowalewskifilip.laboratorium09.pl.imiajd.kowalewski;
import pl.edu.uwm.wmii.kowalewskifilip.laboratorium09.pl.imiajd.kowalewski.*;
import java.util.*;
import java.time.LocalDate;

public class Student extends Osoba implements Clonable, Comparable<Osoba>{
    private double sredniaOcen;

    public Student(String nazwisko, LocalDate dataUrodzenia, double sredniaOcen){
        super(nazwisko, dataUrodzenia);
        this.sredniaOcen = sredniaOcen;
    }

    public double getSredniaOcen(){
        return sredniaOcen;
    }

    public String toString(){
        String a;
        a = super.toString();
        int n = a.length() - 1;
        a = a.substring(0, n);
        a = a + " srenia: " + sredniaOcen + " ]";
        return a;
    }

    public int compareTo(Object obj){
        int a;

        a = super.compareTo(obj);
        if(a == 0){
            if(sredniaOcen > ((Student) obj).sredniaOcen) a++;
            else if(sredniaOcen < ((Student) obj).sredniaOcen) a--;
            else a = a + 0;
        }
        return a;
    }
}
