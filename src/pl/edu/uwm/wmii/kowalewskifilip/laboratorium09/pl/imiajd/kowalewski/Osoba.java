package pl.edu.uwm.wmii.kowalewskifilip.laboratorium09.pl.imiajd.kowalewski;

import java.time.LocalDate;

public class Osoba implements Cloneable, Comparable<Osoba>{

    private String nazwisko;
    private LocalDate dataUrodzenia;
    String className = this.getClass().getSimpleName();


    public Osoba(String nazwisko, LocalDate dataUrodzenia){
        this.nazwisko = nazwisko;
        this.dataUrodzenia = dataUrodzenia;
    }

    String getNazwisko(){
        return nazwisko;
    }

    LocalDate getDataUrodzenia(){
        return dataUrodzenia;
    }

    public String toString(){
        return className + " [" + getNazwisko() + " " + getDataUrodzenia() + "]";
    }


    public boolean equals(Object obj){
        Osoba x = (Osoba) obj;
        if(!nazwisko.equals(x.nazwisko) || !dataUrodzenia.equals(x.dataUrodzenia)) return false;
        return true;
    }

    public int compareTo(Object obj){
        int a = 0;
        Osoba x = (Osoba) obj;
        a = nazwisko.compareTo(x.nazwisko);
        if(a == 0) a = dataUrodzenia.compareTo(x.dataUrodzenia) + a;
        return a;
    }

}




