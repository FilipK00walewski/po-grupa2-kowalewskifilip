package pl.edu.uwm.wmii.kowalewskifilip.laboratorium09;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Z3{
    public static void main(String[] args) throws FileNotFoundException{
        File file = new File("G:\\git2\\src\\pl\\edu\\uwm\\wmii\\kowalewskifilip\\laboratorium09\\tekst");
        Scanner in = new Scanner(file);

        ArrayList<String> tekst = new ArrayList<String>();

        while(in.hasNextLine()) {
            String linia = in.nextLine();
            tekst.add(linia);
        }

        Collections.sort(tekst);

        System.out.println("Po sortowaniu:");
        for(String linia: tekst){
            System.out.println(linia);
        }
    }

}

