package pl.edu.uwm.wmii.kowalewskifilip.laboratorium09;

import java.util.*;
import java.time.LocalDate;
import pl.edu.uwm.wmii.kowalewskifilip.laboratorium09.pl.imiajd.kowalewski.*;

public class TestStudent {
    public static void main(String[] args) {

        ArrayList<Student> grupa = new ArrayList<Student>();

        grupa.add(new Student("Nowak", LocalDate.of(1999, 10, 14), 6));
        grupa.add(new Student("Kowalski", LocalDate.of(1999, 10, 4), 3));
        grupa.add(new Student("Wojtyla", LocalDate.of(1930, 2, 4), 1));
        grupa.add(new Student("Nowak", LocalDate.of(1999, 10, 14), 5));
        grupa.add(new Student("Tusk", LocalDate.of(1990, 11, 15), 5.1));
        grupa.add(new Student("Kowalewski", LocalDate.of(1997, 6, 30), 3));


        System.out.println("Przed sortowaniu: " + grupa);

        Collections.sort(grupa, new Comparator<Student>() {
            @Override
            public int compare(Student o1, Student o2) {
                return o1.compareTo(o2);
            }
        });
        System.out.println("Po sortowaniu: " + grupa);

    }
}






