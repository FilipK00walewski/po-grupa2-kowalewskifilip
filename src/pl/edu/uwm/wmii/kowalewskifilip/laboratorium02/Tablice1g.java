package pl.edu.uwm.wmii.kowalewskifilip.laboratorium02;

import java.util.Scanner;
import java.util.Random;


public class Tablice1g {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Random generator = new Random();

        System.out.println("podaj rozmiar tablicy: ");
        int n = input.nextInt();

        int tab[] = new int[n];

        int lewy, prawy;
        lewy = input.nextInt();
        prawy = input.nextInt();

        for(int i=0; i<n; i++){
            tab[i]=generator.nextInt()%1000;
        }

        for(int i=0; i<n; i++){
            System.out.print(tab[i] + " ");
        }

        int a, x=0;
        for(int i=lewy-1; i<prawy-1; i++){
            a=tab[i];
            tab[i]=tab[prawy-1-x];
            tab[prawy-1-x]=a;

            if(i==(prawy-1-x)) break;

            x++;
        }

        System.out.print("\n");
        for(int i=0; i<n; i++){
            System.out.print(tab[i] + " ");
        }


    }
}