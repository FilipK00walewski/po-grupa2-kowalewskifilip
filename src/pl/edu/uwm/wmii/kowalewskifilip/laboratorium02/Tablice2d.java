package pl.edu.uwm.wmii.kowalewskifilip.laboratorium02;

import java.util.Scanner;
import java.util.Random;


public class Tablice2d {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        System.out.println("podaj rozmiar tablicy <0, 100>: ");
        int n = input.nextInt();

        if (n >= 1 && n <= 100) {
            int a, b;
            int tab[] = new int[n];

            generuj(tab, n, -999, 999);

            a = sumaDodatnich(tab);
            b = sumaUjemnych(tab);

            System.out.println("suma dodatnich: " + b + " suma ujemnych: " + a);

        }

        else System.out.println("error");

    }

    public static void generuj(int tab[], int n, int min, int max) {
        Random generator = new Random();
        int x;
        if (min >= 0) {
            x = max - min + 1;
        }
        else x = (-1)*min + max + 1;

        for (int i = 0; i < n; i++) {
            tab[i] = generator.nextInt(x) + min;
        }
    }

    public static void wyswietl(int tab[]){
        for (int i = 0; i < tab.length; i++) {
            System.out.print(tab[i] + " ");
        }
        System.out.println("\n");
    }

    public static int sumaDodatnich (int tab[]){
        int a = 0;
        for (int i = 0; i < tab.length; i++) {
            if(tab[i] > 0) a = a + tab[i];
        }
        return a;
    }

    public static int sumaUjemnych (int tab[]){
        int a = 0;
        for (int i = 0; i < tab.length; i++) {
            if(tab[i] < 0) a = a + tab[i];
        }
        return a;
    }


}