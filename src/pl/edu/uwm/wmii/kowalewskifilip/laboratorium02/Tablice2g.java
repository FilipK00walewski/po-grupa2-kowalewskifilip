package pl.edu.uwm.wmii.kowalewskifilip.laboratorium02;

import java.util.Scanner;
import java.util.Random;

public class Tablice2g {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);


        System.out.println("podaj rozmiar tablicy <0, 100>: ");
        int n = input.nextInt();

        System.out.println("lewy: ");
        int lewy = input.nextInt();

        System.out.println("prawy: ");
        int prawy = input.nextInt();

        if (n >= 1 && n <= 100 && lewy >= 1 && prawy > lewy && prawy <= n) {
            int tab[] = new int[n];

            generuj(tab, n, -999, 999);

            wyswietl(tab);
            odwrocFragment(tab, lewy, prawy);
            wyswietl(tab);

        }
        else System.out.println("error");
    }

    public static void generuj(int tab[], int n, int min, int max) {
        Random generator = new Random();
        int x;
        if (min >= 0) {
            x = max - min + 1;
        }
        else x = (-1)*min + max + 1;

        for (int i = 0; i < n; i++) {
            tab[i] = generator.nextInt(x) + min;
        }
    }

    public static void wyswietl(int tab[]){
        for (int i = 0; i < tab.length; i++) {
            System.out.print(tab[i] + " ");
        }
        System.out.println("\n");
    }

    public static void odwrocFragment (int tab[] , int lewy, int prawy){
        int a, x=0;
        for(int i=lewy-1; i<prawy-1; i++){
            a=tab[i];
            tab[i]=tab[prawy-1-x];
            tab[prawy-1-x]=a;

            if(i==(prawy-1-x)) break;

            x++;
        }
    }
}