package pl.edu.uwm.wmii.kowalewskifilip.laboratorium02;

import java.util.Scanner;
import java.util.Random;


public class Tablice1b {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Random generator = new Random();

        System.out.println("podaj rozmiar tablicy: ");
        int n = input.nextInt();

        int tab[] = new int[n];

        int d=0, u=0, z=0;

        for(int i=0; i<n; i++){
            tab[i]=generator.nextInt()%1000;
            if(tab[i]>0)d++;
            else if(tab[i]<0) u++;
            else z++;
        }

        System.out.println("dodatnie: " + d + " ujemne: " + u + " zera: " + z);
    }
}