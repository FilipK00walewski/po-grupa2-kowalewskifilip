package pl.edu.uwm.wmii.kowalewskifilip.laboratorium01;

import javax.lang.model.type.NullType;
import java.util.Scanner;

public class Zadanie2_4 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("podaj ilosc liczb ");
        int n = input.nextInt();

        double b, min=0, max=0;

        for(int i=1; i<=n; i++){
            System.out.println("podaj " + i +" liczbe");
            b = input.nextDouble();
            if(i==0){
                max=b;
                min=b;
            }

            if(b>max) max=b;
            if(b<min) min=b;

        }

        System.out.println("max: " + max);
        System.out.println("min: " + min);

    }
}