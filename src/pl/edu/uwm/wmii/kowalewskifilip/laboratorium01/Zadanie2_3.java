package pl.edu.uwm.wmii.kowalewskifilip.laboratorium01;

import java.util.Scanner;

public class Zadanie2_3 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("podaj ilosc liczb ");
        int n = input.nextInt();

        double b;
        double ujemne=0, zera=0, dodatnie=0;

        for(int i=1; i<=n; i++){
            System.out.println("podaj " + i +" liczbe");
            b = input.nextDouble();

            if(b>0) dodatnie++;
            else if(b<0) ujemne++;
            else zera++;
        }

        System.out.println("dodanie: " + dodatnie);
        System.out.println("zera: " + zera);
        System.out.println("ujemne: " + ujemne);
    }
}