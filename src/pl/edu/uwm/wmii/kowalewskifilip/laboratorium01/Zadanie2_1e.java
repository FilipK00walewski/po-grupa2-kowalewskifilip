package pl.edu.uwm.wmii.kowalewskifilip.laboratorium01;

import java.util.Scanner;

public class Zadanie2_1e {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("podaj ilosc liczb ");
        int n = input.nextInt();

        int a=0, b, silnia=1, potega=2;
        for(int i=0; i<n; i++){
            System.out.println("podaj " + (i+1) +" liczbe");
            b = input.nextInt();
            if(b>potega && b<silnia) a++;
            System.out.println(silnia+ "   " +potega);
            silnia=silnia*(i+1);
            potega=potega*2;

        }
        System.out.println("ilosc liczb: " + a);
    }
}