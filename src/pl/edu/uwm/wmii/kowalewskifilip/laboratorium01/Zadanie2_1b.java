package pl.edu.uwm.wmii.kowalewskifilip.laboratorium01;

import java.util.Scanner;

public class Zadanie2_1b {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("podaj ilosc liczb ");
        int n = input.nextInt();

        int a=0, b;
        for(int i=0; i<n; i++){
            System.out.println("podaj " + (i+1) +" liczbe");
            b = input.nextInt();
            if(b%3==0 || b%5==0) a++;
        }
        System.out.println("ilosc liczb podzielnych przez 3 i 5: " + a);
    }
}