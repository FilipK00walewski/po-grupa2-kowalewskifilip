package pl.edu.uwm.wmii.kowalewskifilip.laboratorium01;

import java.util.Scanner;

public class Zadanie1_2 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("podaj ilosc liczb ");
        int n = input.nextInt();
        double[] tab = new double[n];

        double b, a=0;
        for(int i=0; i<n; i++){
            System.out.println("podaj " + (i+1) +" liczbe");
            b = input.nextInt();
            tab[i] = b;
        }

        a = tab[0];
        tab[0] = tab[n-1];
        tab[n-1] = a;
        System.out.println("\n");

        for(int i=0; i<n; i++){
            System.out.print(tab[i] + " ");
        }

    }
}
