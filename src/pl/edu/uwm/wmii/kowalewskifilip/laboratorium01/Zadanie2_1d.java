package pl.edu.uwm.wmii.kowalewskifilip.laboratorium01;

import java.util.Scanner;

public class Zadanie2_1d {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("podaj ilosc liczb ");
        int n = input.nextInt();

        int tab[] = new int[n];

        int a=0, b, x, y;
        for(int i=0; i<n; i++){
            tab[i] = input.nextInt();
        }

        for(int i=1; i<n-1; i++){
            if (tab[i] < ((tab[i - 1] + tab[i + 1]) / 2)) a++;
        }

        System.out.println("ilosc liczb : " + a);
    }
}