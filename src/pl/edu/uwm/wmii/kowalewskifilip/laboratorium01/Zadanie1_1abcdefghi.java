package pl.edu.uwm.wmii.kowalewskifilip.laboratorium01;

import java.util.Scanner;

public class Zadanie1_1abcdefghi {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int a, suma = 0, iloczyn=1;
        System.out.println("podaj ilosc liczb ");
        int n = input.nextInt();

        // a)
        for(int i = 0; i < n; i++){
            System.out.println("podaj "+ (i+1) +" liczbe ");
            a = input.nextInt();
            suma=suma+a;
        }
        System.out.println("Suma liczb: "+ suma);

        // b)
        for(int i = 0; i < n; i++){
            System.out.println("podaj "+ (i+1) +" liczbe ");
            a = input.nextInt();
            iloczyn = iloczyn*a;
        }

        System.out.println("iloczyn liczb: "+ iloczyn);



        // c)
        suma=0;
        for(int i = 0; i < n; i++){
            System.out.println("podaj "+ (i+1) +" liczbe ");
            a = input.nextInt();
            if(a<0) a=a*(-1);
            suma = suma+a;
        }
        System.out.println("Suma wb liczb: "+ suma);


        // d)
        double suma2=0;
        double b;
        for(int i = 0; i < n; i++){
            System.out.println("podaj "+ (i+1) +" liczbe ");
            b = input.nextInt();
            if(b<0) b=b*(-1);
            b=Math.sqrt(b);
            suma2=suma2+b;
        }
        System.out.println("Suma pierwiastkow liczb: "+ suma2);


        // e)
        iloczyn=1;
        for(int i = 0; i < n; i++){
            System.out.println("podaj "+ (i+1) +" liczbe ");
            a = input.nextInt();
            if(a<0) a=a*(-1);
            iloczyn = iloczyn*a;
        }

        System.out.println("iloczyn wb liczb: "+ iloczyn);


        // f)
        suma=0;
        for(int i = 0; i < n; i++){
            System.out.println("podaj "+ (i+1) +" liczbe ");
            a = input.nextInt();
            a=a*a;
            suma = suma+a;
        }
        System.out.println("Suma kwadratow liczb: "+ suma);

        // g)
        suma=0;
        iloczyn=1;
        for(int i = 0; i < n; i++){
            System.out.println("podaj "+ (i+1) +" liczbe ");
            a = input.nextInt();
            suma=suma+a;
            iloczyn=iloczyn*a;
        }
        System.out.println("suma: "+ suma + " iloczyn: " + iloczyn);


        // h)
        suma=0;
        for(int i = 0; i < n; i++){
            System.out.println("podaj "+ (i+1) +" liczbe ");
            a = input.nextInt();
            if(i%2==1) a=a*(-1);
            suma = suma+a;
        }
        System.out.println("h) "+ suma);


        // i)
        double suma3=0;
        double silnia=1;
        double c;
        for(int i = 0; i < n; i++){
            silnia=silnia*(i+1);
            System.out.println("podaj "+ (i+1) +" liczbe ");
            c = input.nextInt();
            if(i%2==0) c=c*(-1);
            suma3 = suma3 + c/silnia;
        }
        System.out.println("i) "+ suma3);


    }
}