package pl.edu.uwm.wmii.kowalewskifilip.laboratorium01;

import java.util.Scanner;

public class Zadanie2_1g {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("podaj ilosc liczb ");
        int n = input.nextInt();

        int a=0, b;
        for(int i=1; i<=n; i++){
            System.out.println("podaj " + i +" liczbe");
            b = input.nextInt();
            if(b%2!=0 && b>=0) a++;
        }
        System.out.println("ilosc liczb: " + a);
    }
}