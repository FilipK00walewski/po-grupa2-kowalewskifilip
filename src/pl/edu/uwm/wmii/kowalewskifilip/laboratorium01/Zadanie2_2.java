package pl.edu.uwm.wmii.kowalewskifilip.laboratorium01;

import java.util.Scanner;

public class Zadanie2_2 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("podaj ilosc liczb ");
        int n = input.nextInt();

        double b;
        double suma=0;

        for(int i=1; i<=n; i++){
            System.out.println("podaj " + i +" liczbe");
            b = input.nextDouble();
            if(b>0) suma=suma+b;
        }

        System.out.println("podwojona suma liczb dodatnich: " + 2*suma);
    }
}