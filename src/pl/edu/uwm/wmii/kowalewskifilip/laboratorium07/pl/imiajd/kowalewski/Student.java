package pl.edu.uwm.wmii.kowalewskifilip.laboratorium07.pl.imiajd.kowalewski;
import pl.edu.uwm.wmii.kowalewskifilip.laboratorium07.pl.imiajd.kowalewski.Osoba;

public class Student extends Osoba
{
    public Student(String nazwisko, String kierunek, String[] imiona, java.time.LocalDate dU, boolean plec, double sredniaOcen)
    {
        super(nazwisko, imiona, dU, plec);
        this.kierunek = kierunek;
        this.sredniaOcen = sredniaOcen;
    }

    public double getSredniaOcen(){
        return sredniaOcen;
    }

    public void setSredniaOcen(double a){
        sredniaOcen = a;
    }

    public String getOpis()
    {
        String a;
        a = "student na kierunku " + kierunek;
        return a;
    }

    private String kierunek;
    private double sredniaOcen;
}