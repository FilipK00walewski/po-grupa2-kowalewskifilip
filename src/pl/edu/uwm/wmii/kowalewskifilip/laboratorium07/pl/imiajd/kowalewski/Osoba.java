package pl.edu.uwm.wmii.kowalewskifilip.laboratorium07.pl.imiajd.kowalewski;


public abstract class Osoba
{
    public Osoba(String nazwisko, String[] imiona, java.time.LocalDate dU, boolean plec)
    {
        this.nazwisko = nazwisko;
        this.imiona = imiona;
        dataUrodzenia = dU;
        this.plec = plec;

    }

    public abstract String getOpis();

    public String getNazwisko()
    {
        return nazwisko;
    }

    public String getImiona() {
        String a = "";
        for (int i = 0; i < imiona.length; i++) a = a + imiona[i] + " ";
        return a;
    }

    public java.time.LocalDate getDataUrodzenia(){
        return dataUrodzenia;
    }

    public boolean getPlec(){
        return plec;
    }

    private String nazwisko;
    private String[] imiona;
    private java.time.LocalDate dataUrodzenia;
    private boolean plec;
}