package pl.edu.uwm.wmii.kowalewskifilip.laboratorium07.pl.imiajd.kowalewski;

public abstract class Instrument {

    public Instrument(String producent, java.time.LocalDate rokProdukcji) {
        this.producent = producent;
        this.rokProdukcji = rokProdukcji;
    }

    public String getProducent() {
        return producent;
    }

    public java.time.LocalDate getRokProdukcji() {
        return rokProdukcji;
    }

    public abstract String dzwiek();

    public String toString() {
        return "meoda toString dziala";
    }

    public void equals(){
        System.out.print("equals");
    }


    String producent;
    java.time.LocalDate rokProdukcji;

}
