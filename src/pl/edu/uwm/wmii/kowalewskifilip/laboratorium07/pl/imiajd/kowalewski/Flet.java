package pl.edu.uwm.wmii.kowalewskifilip.laboratorium07.pl.imiajd.kowalewski;
import pl.edu.uwm.wmii.kowalewskifilip.laboratorium07.pl.imiajd.kowalewski.Instrument;
import java.time.LocalDate;

public class Flet extends Instrument{

    public Flet(String producent, java.time.LocalDate dataProdukcji){
        super(producent, dataProdukcji);
    }

    public String dzwiek(){
        return "dzwiek fletu";
    }

}
