package pl.edu.uwm.wmii.kowalewskifilip.laboratorium07.pl.imiajd.kowalewski;

import pl.edu.uwm.wmii.kowalewskifilip.laboratorium07.pl.imiajd.kowalewski.Osoba;

public class Pracownik extends Osoba
{
    public Pracownik(String nazwisko, double pobory, String[] imiona, java.time.LocalDate dU, boolean plec, java.time.LocalDate dZ)
    {
        super(nazwisko, imiona, dU, plec);
        this.pobory = pobory;
        dataZatrudnienia = dZ;
    }

    public double getPobory()
    {
        return pobory;
    }

    public java.time.LocalDate getDataZatrudnienia(){
        return dataZatrudnienia;
    }

    public String getOpis()
    {
        return String.format("pracownik z pensją %.2f zł", this.pobory);
    }

    private double pobory;
    private java.time.LocalDate dataZatrudnienia;
}

