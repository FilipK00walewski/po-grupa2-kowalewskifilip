package pl.edu.uwm.wmii.kowalewskifilip.laboratorium07.pl.imiajd.kowalewski;
import pl.edu.uwm.wmii.kowalewskifilip.laboratorium07.pl.imiajd.kowalewski.Instrument;
import java.time.LocalDate;

public class Skrzypce extends Instrument{

    public Skrzypce(String producent, java.time.LocalDate rokProdukcji){
        super(producent, rokProdukcji);
    }

    public String dzwiek(){
        return "dzwiek skrzypiec";
    }
}
