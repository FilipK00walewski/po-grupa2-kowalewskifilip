package pl.edu.uwm.wmii.kowalewskifilip.laboratorium07;

import pl.edu.uwm.wmii.kowalewskifilip.laboratorium07.pl.imiajd.kowalewski.Osoba;
import pl.edu.uwm.wmii.kowalewskifilip.laboratorium07.pl.imiajd.kowalewski.Pracownik;
import pl.edu.uwm.wmii.kowalewskifilip.laboratorium07.pl.imiajd.kowalewski.Student;


import java.time.LocalDate;
import java.util.*;

public class TestOsoba
{
    public static void main(String[] args)
    {
        Osoba[] ludzie = new Osoba[2];

        String[] imiona = new String[2];
        imiona[0] = "Jan";
        imiona[1] = "Piotr";

        String[] imiona1 = new String[1];
        imiona1[0] = "Krzysztof";

        ludzie[0] = new Pracownik("Kowalski", 5000, imiona, LocalDate.of(1957, 07, 15), true, LocalDate.of(1997, 06, 15));
        ludzie[1] = new Student("Pupecki", "informatyka", imiona1, LocalDate.of(1997, 06, 30), true, 4.32);


        for (Osoba p : ludzie) {
            System.out.println(p.getImiona() + p.getNazwisko() + ": " + p.getOpis());
        }
    }
}







