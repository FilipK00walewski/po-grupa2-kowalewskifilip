package pl.edu.uwm.wmii.kowalewskifilip.laboratorium07;
import pl.edu.uwm.wmii.kowalewskifilip.laboratorium07.pl.imiajd.kowalewski.Flet;
import pl.edu.uwm.wmii.kowalewskifilip.laboratorium07.pl.imiajd.kowalewski.Instrument;
import pl.edu.uwm.wmii.kowalewskifilip.laboratorium07.pl.imiajd.kowalewski.Fortepian;
import pl.edu.uwm.wmii.kowalewskifilip.laboratorium07.pl.imiajd.kowalewski.Skrzypce;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class TestInstrumenty {
    public static void main(String[] args){

        List<Instrument> orkiestra = new ArrayList<Instrument>();

        orkiestra.add(new Flet("Sony", LocalDate.ofYearDay(1900, 1)));
        orkiestra.add(new Flet("Asus", LocalDate.ofYearDay(1600, 1)));
        orkiestra.add(new Skrzypce("Oniszko", LocalDate.ofYearDay(1900, 1)));
        orkiestra.add(new Fortepian("xxx", LocalDate.ofYearDay(1200, 1)));
        orkiestra.add(new Skrzypce("Audi", LocalDate.ofYearDay(1500, 1)));


        for (Instrument p : orkiestra) {
            System.out.println(p.dzwiek());
        }

    }
}








