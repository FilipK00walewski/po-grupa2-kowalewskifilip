package pl.edu.uwm.wmii.kowalewskifilip.laboratorium04;

import java.util.ArrayList;
import java.util.Scanner;

public class Zadanie5 {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);

        System.out.println("podaj rozmiar listy: ");
        int n1 = input.nextInt();
        ArrayList<Integer> a = new ArrayList<Integer>(n1);
        int x;
        for(int i = 0; i < n1; i++){
            x = input.nextInt();
            a.add(x);
        }

        reverse(a);
        System.out.println(a);

    }

    public static void reverse(ArrayList<Integer> a) {
        int x = 0;

        ArrayList<Integer> c = new ArrayList<Integer>(a.size());

        for(int i = a.size() - 1; i >= 0; i--){
            c.add(a.get(i));
            a.remove(i);
        }

        for(int i = 0; i < c.size(); i++){
            a.add(c.get(i));
        }


    }
}
