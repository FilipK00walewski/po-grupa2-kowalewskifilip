package pl.edu.uwm.wmii.kowalewskifilip.laboratorium04;

import java.util.ArrayList;
import java.util.Scanner;

public class Zadanie2 {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);


        System.out.println("podaj rozmiar 1 listy: ");
        int n1 = input.nextInt();
        ArrayList<Integer> a = new ArrayList<Integer>(n1);
        int x;
        for(int i = 0; i < n1; i++){
            x = input.nextInt();
            a.add(x);
        }
        System.out.println("podaj rozmiar 2 listy: ");
        int n2 = input.nextInt();
        ArrayList<Integer> b = new ArrayList<Integer>(n2);
        for(int i = 0; i < n2; i++){
            x = input.nextInt();
            b.add(x);
        }
        ArrayList<Integer> c = new ArrayList<Integer>(n1 + n2);
        c = marge(a, b);
        System.out.println(c);
    }

    public static ArrayList<Integer> marge(ArrayList<Integer> a, ArrayList<Integer> b){
        int x, y = a.size() + b.size();

        ArrayList<Integer> c = new ArrayList<Integer>(y);

        if(a.size() < b.size()) x = a.size();
        else if(a.size() > b.size()) x = b.size();
        else{
            x = a.size();
            y = x;
        }

        for(int i = 0; i < x ; i++){
            if(a.get(i) != null) {
                c.add(a.get(i));
            }
            if(b.get(i) != null){
                c.add(b.get(i));
            }
        }
        for(int i = x; i < y - x; i++){
            if(a.size() < b.size()) c.add(b.get(i));
            else c.add(a.get(i));
        }
        return c;
    }

}
