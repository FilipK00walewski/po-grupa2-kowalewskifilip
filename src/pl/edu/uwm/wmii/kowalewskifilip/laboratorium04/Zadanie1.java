package pl.edu.uwm.wmii.kowalewskifilip.laboratorium04;

import java.util.ArrayList;
import java.util.Scanner;

public class Zadanie1 {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);


        System.out.println("podaj rozmiar 1 listy: ");
        int n1 = input.nextInt();
        ArrayList<Integer> a = new ArrayList<Integer>(n1);
        int x;
        for(int i = 0; i < n1; i++){
            x = input.nextInt();
            a.add(x);
        }
        System.out.println("podaj rozmiar 2 listy: ");
        int n2 = input.nextInt();
        ArrayList<Integer> b = new ArrayList<Integer>(n2);
        for(int i = 0; i < n1; i++){
            x = input.nextInt();
            b.add(x);
        }
        ArrayList<Integer> c = new ArrayList<Integer>(n1 + n2);
        c = append(a, b);
        System.out.println(c);
    }

    public static ArrayList<Integer> append(ArrayList<Integer> a, ArrayList<Integer> b){
        int x;
        for(int i = 0; i < b.size(); i++){
            x = b.get(i);
            a.add(x);
        }
        return a;
    }

}
