package pl.edu.uwm.wmii.kowalewskifilip.laboratorium04;

import java.util.ArrayList;
import java.util.Scanner;

public class Zadanie3 {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);


        System.out.println("podaj rozmiar 1 listy: ");
        int n1 = input.nextInt();
        ArrayList<Integer> a = new ArrayList<Integer>(n1);
        int x;
        for(int i = 0; i < n1; i++){
            x = input.nextInt();
            a.add(x);
        }
        System.out.println("podaj rozmiar 2 listy: ");
        int n2 = input.nextInt();
        ArrayList<Integer> b = new ArrayList<Integer>(n2);
        for(int i = 0; i < n2; i++){
            x = input.nextInt();
            b.add(x);
        }
        ArrayList<Integer> c = new ArrayList<Integer>(n1 + n2);
        c = margeSorted(a, b);
        System.out.println(c);
    }

    public static ArrayList<Integer> margeSorted(ArrayList<Integer> a, ArrayList<Integer> b){
        int x = 0, n = a.size() + b.size(), min;

        ArrayList<Integer> c = new ArrayList<Integer>(n);

        for(int i = 0; i < b.size(); i++) a.add(b.get(i));

        for(int j = 0; j < a.size(); ) {
            min = a.get(0);
            for (int i = 0; i < a.size(); i++) {
                if (a.get(i) < min) {
                    min = a.get(i);
                    x = i;
                }
            }
            c.add(min);
            a.remove(x);
            x = 0;
        }
        return c;
    }

}
