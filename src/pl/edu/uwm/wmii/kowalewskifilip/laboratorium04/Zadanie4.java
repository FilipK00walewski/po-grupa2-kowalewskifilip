package pl.edu.uwm.wmii.kowalewskifilip.laboratorium04;

import java.util.ArrayList;
import java.util.Scanner;

public class Zadanie4 {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);


        System.out.println("podaj rozmiar listy: ");
        int n1 = input.nextInt();
        ArrayList<Integer> a = new ArrayList<Integer>(n1);
        int x;
        for(int i = 0; i < n1; i++){
            x = input.nextInt();
            a.add(x);
        }

        ArrayList<Integer> c = new ArrayList<Integer>(n1);
        c = reversed(a);
        System.out.println(c);
    }

    public static ArrayList<Integer> reversed(ArrayList<Integer> a) {
        int x = 0;

        ArrayList<Integer> c = new ArrayList<Integer>(a.size());

        for(int i = a.size() - 1; i >= 0; i--){
            c.add(a.get(i));
        }
        return c;
    }
}
