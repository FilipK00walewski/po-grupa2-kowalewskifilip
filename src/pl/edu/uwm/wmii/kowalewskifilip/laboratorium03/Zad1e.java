package pl.edu.uwm.wmii.kowalewskifilip.laboratorium03;


import javax.lang.model.type.NullType;
import java.util.Scanner;


public class Zad1e {
    public static final int n = 100;
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        String str, subStr;
        int tab[] = new int[n];

        int y = 0;
        System.out.println("Podaj wyraz: ");
        str = input.nextLine();
        System.out.println("Podaj 2 wyraz: ");
        subStr = input.nextLine();

        tab = countSubStr(str, subStr);

        for(int i = 0; i < n; i++){
            if(tab[i] == 0) break;
            System.out.print(tab[i] + " ");
        }
    }


    public static int[] countSubStr(String str, String subStr){

        int z = 0, x = 0;
        int tab[] = new int[n];

        for(int i = 0; i < str.length(); i++){
            if(str.charAt(i) == subStr.charAt(z)) z++;
            else z = 0;
            if(z == subStr.length()) {
                z = 0;
                tab[x] = i + 2 - subStr.length();
                x++;
            }
        }
        return tab;
    }
}