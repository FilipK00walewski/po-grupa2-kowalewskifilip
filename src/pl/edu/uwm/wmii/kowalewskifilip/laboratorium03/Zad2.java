import javax.lang.model.type.NullType;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Zad2{
    public static void main(String[] args) throws FileNotFoundException{
        int x;
        Scanner input = new Scanner(System.in);
        String nazwaPliku, znak;
        System.out.println("podaj sciezke pliku: ");
        nazwaPliku = input.nextLine();
        System.out.println("podaj znak: ");
        znak = input.nextLine();

        x = pliki(nazwaPliku, znak.charAt(0));
        System.out.println("Liczba znakow w pliku: " + x);
    }

    public static int pliki(String nazwaPliku, char znak) throws FileNotFoundException{
        int x = 0;
        String linia;
        File file = new File(nazwaPliku);
        Scanner in = new Scanner(file);

        while(in.hasNextLine()){
            linia=in.nextLine();
            for(int i = 0; i < linia.length(); i++){
                if(linia.charAt(i) == znak) x++;
            }
        }

        return x;
    }


}
