package pl.edu.uwm.wmii.kowalewskifilip.laboratorium03;

import java.math.BigDecimal;
import java.util.Scanner;

public class Zad5 {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        System.out.println("Podaj kapital poczatkowy, stope procentowa i dlugosc okresu oszczedzania: ");
        int  k = input.nextInt();
        int  p = input.nextInt();
        int  n = input.nextInt();

        bank(k, p, n);

    }

    public static void bank(int k, int p, int n){

        BigDecimal suma = BigDecimal.valueOf(k);
        BigDecimal odsetki = BigDecimal.valueOf(1);
        BigDecimal P = BigDecimal.valueOf(p);
        BigDecimal sto = BigDecimal.valueOf(100);


        for(int i = 0; i < n; i++){
            odsetki = odsetki.multiply(suma).multiply(P).divide(sto);

            suma = suma.add(odsetki);
        }

        System.out.println(suma);
    }

}