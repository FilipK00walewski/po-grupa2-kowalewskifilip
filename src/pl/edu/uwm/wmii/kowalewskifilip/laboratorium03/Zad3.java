import javax.lang.model.type.NullType;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Zad3{
    public static void main(String[] args) throws FileNotFoundException{
        int x;
        Scanner input = new Scanner(System.in);
        String nazwaPliku, str, str2;
        System.out.println("podaj sciezke pliku: ");
        nazwaPliku = input.nextLine();
        System.out.println("podaj wyraz: ");
        str = input.nextLine();


        x = pliki(nazwaPliku, str);
        System.out.println("Liczba wyrazow w pliku: " + x);
    }

    public static int pliki(String nazwaPliku, String subStr) throws FileNotFoundException{
        int x = 0;
        int z = 0;
        String linia;
        File file = new File(nazwaPliku);
        Scanner in = new Scanner(file);

        while(in.hasNextLine()){
            linia=in.nextLine();

            for(int i = 0; i < linia.length(); i++){
                if(linia.charAt(i) == subStr.charAt(z)) z++;
                else z = 0;
                if(z == subStr.length()) {
                    x++;
                    z = 0;
                }
            }
        }

        return x;
    }


}
