package pl.edu.uwm.wmii.kowalewskifilip.laboratorium03;

import java.math.BigInteger;
import java.util.Scanner;

public class Zad4 {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        System.out.println("Podaj rozmiar szachownicy: ");
        int  n = input.nextInt();
        szachownica(n);
    }

    public static void szachownica(int n){
        n = n * n;
        BigInteger suma = BigInteger.valueOf(0);
        BigInteger a = BigInteger.valueOf(1);
        BigInteger b = BigInteger.valueOf(2);


        for(int i = 0; i < n; i++){
            suma = suma.add(a);
            a = a.multiply(b);
        }

        System.out.println(suma);
    }

}
