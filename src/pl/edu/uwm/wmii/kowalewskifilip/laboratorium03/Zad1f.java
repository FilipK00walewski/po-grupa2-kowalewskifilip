package pl.edu.uwm.wmii.kowalewskifilip.laboratorium03;

import java.util.Scanner;

public class Zad1f {

    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        String a;

        System.out.println("Podaj wyraz: ");
        a = input.nextLine();

        a = change(a);
        System.out.println(a);
    }

    public static String change(String str){
        String S;
        S = new StringBuffer().toString();


        for(int i = 0; i < str.length(); i++){
            if(str.charAt(i) != str.toUpperCase().charAt(i)) {
                S = new StringBuffer(S).append(str.toUpperCase().charAt(i)).toString();
            }
            else{
                S = new StringBuffer(S).append(str.toLowerCase().charAt(i)).toString();
            }
        }

        return S;

    }

}
