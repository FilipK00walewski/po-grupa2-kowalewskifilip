package pl.edu.uwm.wmii.kowalewskifilip.laboratorium03;


import java.util.Scanner;


public class Zad1c {

    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        String str, subStr;

        String y;
        System.out.println("Podaj wyraz: ");
        str = input.nextLine();

        y = middle(str);
        System.out.println("1c) " + y);
    }


    public static String middle(String str){
        char a, b;
        int x = str.length()/2;

        if(str.length() % 2 == 0){
            a = str.charAt(x - 1);
            b = str.charAt(x);
            str = String.valueOf(a) + String.valueOf(b);
        }
        else {
            a = str.charAt(x);
            str = String.valueOf(a);
        }

        return str;
    }

}