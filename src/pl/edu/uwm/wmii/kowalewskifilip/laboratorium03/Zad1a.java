package pl.edu.uwm.wmii.kowalewskifilip.laboratorium03;

import java.util.ArrayList;
import java.util.Scanner;


public class Zad1a {

    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        String str, a;
        char c;
        int y = 0;
        System.out.println("Podaj wyraz: ");
        str = input.nextLine();
        System.out.println("Podaj znak: ");
        a = input.next();
        c = a.charAt(0);

        y = countChar(str, c);
        System.out.println("znak " + c + " wystepuje w wyrazie " + str + " " + y + " razy");
    }

    public static int countChar(String str, char c){
        int x = 0;
        for(int i = 0; i < str.length(); i++){
            if(str.charAt(i) == c) x++;
        }

        return x;
    }


}
