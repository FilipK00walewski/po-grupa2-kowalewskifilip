package pl.edu.uwm.wmii.kowalewskifilip.laboratorium03;


import java.util.Scanner;


public class Zad1b {

    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        String str, subStr;

        int y = 0;
        System.out.println("Podaj wyraz: ");
        str = input.nextLine();
        System.out.println("Podaj 2 wyraz: ");
        subStr = input.nextLine();

        y = countSubStr(str, subStr);
        System.out.println("1b) " + y);
    }


    public static int countSubStr(String str, String subStr){
        int x = 0, z = 0;

        for(int i = 0; i < str.length(); i++){
            if(str.charAt(i) == subStr.charAt(z)) z++;
            else z = 0;
            if(z == subStr.length()) {
                x++;
                z = 0;
            }
        }

        return x;
    }
}