package pl.edu.uwm.wmii.kowalewskifilip.laboratorium03;

import java.util.Scanner;

public class Zad1h {

    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        String a, a1;
        char b;
        int c;

        System.out.println("Podaj liczbe: ");
        a = input.nextLine();
        System.out.println("Podaj znak separatora: ");
        a1 = input.nextLine();
        b = a1.charAt(0);
        System.out.println("Podaj liczbe odstepow: ");
        c = input.nextInt();

        a = nice(a, b, c);
        System.out.println(a);
    }

    public static String nice(String str, char a, int b){
        String S;
        S = new StringBuffer().toString();
        int x = b - str.length()%b;

        for(int i = 0; i < str.length(); i++){

            S = new StringBuffer(S).append(str.charAt(i)).toString();
            x++;
            if(x == b && i != str.length() - 1) {
                S = new StringBuffer(S).append(a).toString();
                x = 0;
            }
        }

        return S;

    }

}

