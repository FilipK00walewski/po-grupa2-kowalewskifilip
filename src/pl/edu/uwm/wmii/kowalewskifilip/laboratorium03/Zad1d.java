package pl.edu.uwm.wmii.kowalewskifilip.laboratorium03;


import java.util.Scanner;


public class Zad1d {

    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        String str, y;
        int n;

        System.out.println("Podaj wyraz: ");
        str = input.nextLine();
        System.out.println("Podaj ilosc powtorzen: ");
        n = input.nextInt();

        y = repeat(str, n);
        System.out.println("1c) " + y);
    }


    public static String repeat(String str, int n){
                String a = str;
                for(int i = 0; i < n - 1; i++){
                    str = str + a;
        }
        return str;
    }

}
