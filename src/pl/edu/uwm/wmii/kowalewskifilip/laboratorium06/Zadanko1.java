package pl.edu.uwm.wmii.kowalewskifilip.laboratorium06;

public class Zadanko1 {
    public static void main(String[] args){
        RachunekBankowy saldo = new RachunekBankowy();
        RachunekBankowy.rocznaStopaProcentowa = 0.04;

        saldo.saver1 = 2000;
        saldo.saver2 = 3000;
        RachunekBankowy.obliczMiesieczneOdsetki(saldo.saver1);
        RachunekBankowy.obliczMiesieczneOdsetki(saldo.saver2);

        RachunekBankowy.rocznaStopaProcentowa = 0.05;

        saldo.saver1 = 2000;
        saldo.saver2 = 3000;
        RachunekBankowy.obliczMiesieczneOdsetki(saldo.saver1);
        RachunekBankowy.obliczMiesieczneOdsetki(saldo.saver2);

    }
}

class RachunekBankowy{
    static double rocznaStopaProcentowa;
    static double saver1, saver2;


    public static void obliczMiesieczneOdsetki(double x){
        double odsetki;
        odsetki = (x * rocznaStopaProcentowa) / 12;
        System.out.println(x + odsetki);
    }
}