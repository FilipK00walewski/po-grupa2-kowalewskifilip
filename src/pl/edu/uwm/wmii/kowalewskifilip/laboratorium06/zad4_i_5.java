package pl.edu.uwm.wmii.kowalewskifilip.laboratorium06;
import pl.edu.uwm.wmii.kowalewskifilip.laboratorium06.pl.imiajd.kowalewski.zad5.*;

public class zad4_i_5 {
    public static void main(String[] args){
        Osoba maciek = new Osoba("pupinski", 2000);
        Osoba marian = new Osoba("wojski", 1900);
        Nauczyciel krzysztof = new Nauczyciel("hak", 1950, 4000);
        Student marcin = new Student("nowak", 2000, "informatyka ogolna");

        System.out.println("nazwisko: " + maciek.getNazwisko());
        System.out.println("rok urodzenia: " + maciek.getRokUrodzenia());

        System.out.print(krzysztof.toString() + "\n");
        System.out.print(marian.toString() + "\n");
        System.out.print(marcin.toString());

    }
}



