package pl.edu.uwm.wmii.kowalewskifilip.laboratorium06;
import pl.edu.uwm.wmii.kowalewskifilip.laboratorium06.pl.imiajd.kowalewski.zad8.BetterRectangle;

public class zad6_7_i_8 {
    public static void main(String[] args){
        BetterRectangle p1 = new BetterRectangle(10, 5);

        System.out.println("pole: " + p1.getArea());
        System.out.println("obwod: " + p1.getPerimeter());
    }
}

