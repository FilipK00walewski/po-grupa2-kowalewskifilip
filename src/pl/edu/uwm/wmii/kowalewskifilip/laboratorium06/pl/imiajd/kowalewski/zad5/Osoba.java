package pl.edu.uwm.wmii.kowalewskifilip.laboratorium06.pl.imiajd.kowalewski.zad5;

public class Osoba{
    private String nazwisko;
    private int rokUrodzenia;

    public Osoba(String nazwisko, int rokUrodzenia){
        this.nazwisko = nazwisko;
        this.rokUrodzenia = rokUrodzenia;
    }

    public String getNazwisko(){ return nazwisko;}
    public int getRokUrodzenia(){ return rokUrodzenia;}

    public String toString(){
        return "Osoba toStrping";
    }

}
