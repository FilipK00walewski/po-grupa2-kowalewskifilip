package pl.edu.uwm.wmii.kowalewskifilip.laboratorium06.pl.imiajd.kowalewski.zad5;
import pl.edu.uwm.wmii.kowalewskifilip.laboratorium06.pl.imiajd.kowalewski.zad5.Osoba;

public class Student extends Osoba {
    String kierunek;

    public Student(String nazwisko, int rokUrodzenia, String kierunek){
        super(nazwisko, rokUrodzenia);
        this.kierunek = kierunek;
    }

    public String getKierunek(){return kierunek;}

    public String toString(){
        return "Student toString";
    }
}