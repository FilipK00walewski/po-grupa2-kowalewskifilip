package pl.edu.uwm.wmii.kowalewskifilip.laboratorium06.pl.imiajd.kowalewski.zad3;

public class Adres{
    private String ulica;
    private int numer_domu;
    private int numer_mieszkania;
    private String miasto;
    String kod_pocztowy;

    public Adres(String ulica, int numer_domu, String miasto, String kod_pocztowy){
        this.ulica = ulica;
        this.numer_domu = numer_domu;
        this.miasto = miasto;
        this.kod_pocztowy = kod_pocztowy;
    }

    public Adres(String ulica, int numer_domu, int a, String miasto, String kod_pocztowy){
        this.ulica = ulica;
        this.numer_domu = numer_domu;
        numer_mieszkania = a;
        this.miasto = miasto;
        this.kod_pocztowy = kod_pocztowy;
    }

    public void pokaz(){
        System.out.println(kod_pocztowy + " " + miasto);
        System.out.println("ul. " + ulica + " " + numer_domu + "/" + numer_mieszkania);
    }

    public boolean przed(Object obj){
        if(obj != kod_pocztowy) return false;
        return true;
    }

}