package pl.edu.uwm.wmii.kowalewskifilip.laboratorium06.pl.imiajd.kowalewski.zad5;

public class Nauczyciel extends Osoba {
    int pensja;

    public Nauczyciel(String nazwisko, int rokUrodzenia, int pensja){
        super(nazwisko, rokUrodzenia);
        this.pensja = pensja;
    }

    public int getPensja(){return pensja;}

    public String toString(){
        return "Nauczyciel toString";
    }
}
