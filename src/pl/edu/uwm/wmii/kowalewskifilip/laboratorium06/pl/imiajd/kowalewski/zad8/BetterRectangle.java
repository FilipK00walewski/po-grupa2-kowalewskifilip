package pl.edu.uwm.wmii.kowalewskifilip.laboratorium06.pl.imiajd.kowalewski.zad8;

import java.awt.*;

public class BetterRectangle extends Rectangle {

    public int getPerimeter(){
        return  2 * (width + height);
    }

    public int getArea(){
        return width * height;
    }

    public BetterRectangle(int a, int b){
        setSize(a, b);
        //zadanie 7
        //super(a, b);
    }

    }
