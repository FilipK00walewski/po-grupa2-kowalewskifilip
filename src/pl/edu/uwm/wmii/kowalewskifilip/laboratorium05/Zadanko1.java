package pl.edu.uwm.wmii.kowalewskifilip.laboratorium05;


public class Zadanko1 {
    public static void main(String[] args){
        RachunekBankowy saver1 = new RachunekBankowy(2000);
        RachunekBankowy.rocznaStopaProcentowa = 0.04;
        saver1.obliczMiesieczneOdsetki();
        RachunekBankowy.rocznaStopaProcentowa = 0.05;
        saver1.obliczMiesieczneOdsetki();
        RachunekBankowy saver2 = new RachunekBankowy(3000);
        RachunekBankowy.rocznaStopaProcentowa = 0.04;
        saver2.obliczMiesieczneOdsetki();
        RachunekBankowy.rocznaStopaProcentowa = 0.05;
        saver2.obliczMiesieczneOdsetki();

    }
}

class RachunekBankowy{
    static double rocznaStopaProcentowa;
    static double saldo;

    public RachunekBankowy(int a){
        saldo = a;
    }

    public static void obliczMiesieczneOdsetki(){
        double odsetki;
        odsetki = (saldo * rocznaStopaProcentowa) / 12;
        System.out.println(saldo + odsetki);
    }
}