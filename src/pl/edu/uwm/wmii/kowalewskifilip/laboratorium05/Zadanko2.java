package pl.edu.uwm.wmii.kowalewskifilip.laboratorium05;

import java.util.Scanner;
import java.util.ArrayList;

public class Zadanko2 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        IntegerSet a = new IntegerSet();
        IntegerSet b = new IntegerSet();

        ArrayList<Boolean> lista2 = new ArrayList<Boolean>();

        int n = 5;
        for(int i = 0; i < n; i++){
            a.lista.add(i);
        }

        for(int i = 0; i < n; i++){
            if (a.lista.get(i) >= 0 && a.lista.get(i) <= 100){
                lista2.add(true);
            }
            else lista2.add(false);
        }



        for(int i = 0; i < n; i++){
            b.lista.add(i * 2);
        }

        a.odczytaj();
        System.out.print(lista2 + "\n");
        b.odczytaj();

        IntegerSet.union(a.lista, b.lista);
        IntegerSet.intersection(b.lista, a.lista);
        a.insertElement(5);
        a.odczytaj();
        a.deleteElement(3);
        a.odczytaj();
        System.out.print("toString: " + a.toSrting() + "\n");
        System.out.print("equals: " + a.equals(b) + "\n");


    }
}

class IntegerSet{
    ArrayList<Integer> lista = new ArrayList<Integer>();


    public IntegerSet(){

    }

    public void odczytaj(){

        for(int i = 0; i < lista.size(); i++){
            System.out.print(lista.get(i));
        }
        System.out.print("\n");
    }
/*
    public void czyNalezy(){
        for(int i = 0; i < lista.size(); i++) {

            int a = Integer.valueOf(lista.get(i));
            if (lista.get(i) == "100" && lista.get(i) >= 0) System.out.print("true ");
            else System.out.print("false ");
        }
    }
*/


    static void union(ArrayList a, ArrayList b){

        ArrayList c = new ArrayList(a.size());

        for (int i = 0; i < a.size(); i++) c.add(a.get(i));

        boolean x = false;
        int n = a.size(), y;
        for (int i = 0; i < b.size(); i++) {
            for (int j = 0; j < a.size(); j++){
                if(b.get(i) == a.get(j)) x = true;
            }
            if(x == false){
                c.add(b.get(i));
                n++;
            }
            x = false;
        }
        System.out.print("Suma mnogosciowa: " + c + "\n");
    }

    static void intersection(ArrayList a, ArrayList b){
        ArrayList c = new ArrayList();

        for (int i = 0; i < b.size(); i++) {
            for (int j = 0; j < a.size(); j++){
                if(b.get(i) == a.get(j)) {
                    c.add(b.get(i));
                    break;
                }
            }
        }
        System.out.print("Iloczyn mnogosciowy: " + c + "\n");
    }

    public void insertElement(int n){
        lista.add(n);
    }

    public void deleteElement(int n){
        lista.remove(n);
    }

    public String toSrting(){
        String a = "";

        for (int i = 0; i < lista.size(); i++){
            a = a + lista.get(i) + " ";
        }
        return a;
    }

    public boolean equals(ArrayList b){
        int w = 0;
        if(lista.size() == b.size()){
            for (int i = 0; i < b.size(); i++){
                if(lista.get(i) == b.get(i)) w++;
            }
            if(w == b.size()) return true;
            else return false;
        }
        else return false;
    }

}
