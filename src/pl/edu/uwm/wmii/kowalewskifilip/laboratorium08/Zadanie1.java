package pl.edu.uwm.wmii.kowalewskifilip.laboratorium08;

import pl.edu.uwm.wmii.kowalewskifilip.laboratorium08.pl.imiajd.kowalewski.Osoba;
import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;
import java.util.Comparator;


public class Zadanie1 {

    public static void main(String[] args){

        Osoba[] grupa = new Osoba[5];

        grupa[0] = new Osoba("Pazdzioch", LocalDate.of(1950, Month.DECEMBER, 30));
        grupa[1] = new Osoba("Boczek", LocalDate.of(1990, Month.APRIL, 5));
        grupa[2] = new Osoba("Kiepski", LocalDate.of(1990, Month.APRIL, 5));
        grupa[3] = new Osoba("Pazdzioch", LocalDate.of(1969, Month.APRIL, 15));
        grupa[4] = new Osoba("Kowalewski", LocalDate.of(1997, Month.JULY, 30));

        System.out.println("Przed sortowaniem: \n");
        wyswietl(grupa);

        Arrays.sort(grupa, new Comparator<Osoba>() {
            @Override
            public int compare(Osoba o1, Osoba o2) {
                return o1.compareTo(o2);
            }
        });

        System.out.println("\nPo sortowaniu: \n");
        wyswietl(grupa);

    }

    public static void wyswietl(Osoba grupa[]){
        for(int i = 0; i < grupa.length; i++){
            System.out.println(grupa[i] );
        }
    }



}












