package pl.edu.uwm.wmii.kowalewskifilip.laboratorium08.pl.imiajd.kowalewski;

interface Comparable{
    public int compareTo(Object obj);
}