package pl.edu.uwm.wmii.kowalewskifilip.laboratorium08.pl.imiajd.kowalewski;

import pl.edu.uwm.wmii.kowalewskifilip.laboratorium08.pl.imiajd.kowalewski.Comparable;

import java.time.LocalDate;
import java.time.Period;

public class Osoba implements Comparable {

    String className = this.getClass().getSimpleName();
    private String nazwisko;
    LocalDate dataUrodzenia;
    LocalDate today = LocalDate.now();




    public Osoba(String nazwisko, LocalDate dataUrodzenia){
        this.nazwisko = nazwisko;
        this.dataUrodzenia = dataUrodzenia;
    }

    public String getNazwisko(){
        return nazwisko;
    }

    public LocalDate getDataUrodzenia(){
        return dataUrodzenia;
    }

    public int ileLat(){
        Period p = Period.between(dataUrodzenia, today);
        return p.getYears();
    }

    public int ileMiesiecy(){
        Period p = Period.between(dataUrodzenia, today);
        return p.getMonths();
    }

    public int ileDni(){
        Period p = Period.between(dataUrodzenia, today);
        return p.getDays();
    }

    public String toString(){
        return className + " [" + getNazwisko() + " " + getDataUrodzenia() + "]" + " Lat: " + ileLat() + " Misesiecy: " + ileMiesiecy() + " Dni: " + ileDni();
    }

    public boolean equals(Object obj){
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;

        Osoba x = (Osoba) obj;
        if(!nazwisko.equals(x.nazwisko) || !dataUrodzenia.equals(x.dataUrodzenia)) return false;
        return true;
    }

    public int compareTo(Object obj){
        return toString().compareTo(obj.toString());

    }

}


